import { Component, OnInit, ViewChild, ElementRef, Compiler } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { DefaultService } from '../../utils/providers/default/default.service';
import { CommonService } from '../../utils/providers/common/common.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.css']
})
export class DefaultComponent implements OnInit {
  base_url: string = environment.apiEndPoint;
  @ViewChild('myInput') myInput: ElementRef;
  @ViewChild('csvInput') csvInput: ElementRef;
  @ViewChild('preProcessingDropDown') preProcessingDropDown: ElementRef;
  @ViewChild('docTypeDropDown') docTypeDropDown: ElementRef;

  imgDisplay: boolean = true;
  loading: boolean = true;
  btnDisabled: boolean = true;
  btnClear: boolean = true;
  defaultTextArea: boolean = false;
  isCSVDisable: boolean = true;
  isOCR: boolean = true;
  extractedContent: string;
  extractedText: string = '';
  result: string = 'Result...';
  defaultOption: string = 'documents';
  freeText: string = '';
  fileName: any;
  fileString: any;
  filter: boolean;
  path: string;
  preProcessingItems: any;
  preProcessingSelectedValue: any = "";
  reportTypeItems: any;
  reportTypeSelectedValue: any = "";
  imageResult: any;
  outputFileName: any;
  downloadableFileName: any;
  SelectedImageAsBase64: any;
  constructor(private router: Router,
    private notify: NotificationsService,
    private defaultService: DefaultService,
    private commonService: CommonService,
    private _compiler: Compiler, private _sanitizer: DomSanitizer) {
    setTimeout(() => {
      this.loading = false;
    }, 1000)
  }

  ngOnInit() {
    // get items for preprocessing dropdown
    this.defaultService.getPreprocessing().subscribe(
      items => this.preProcessingItems = items
    );

    //get items for report type dropdown
    // this.defaultService.getReportType().subscribe(
    //   reportItems => this.reportTypeItems=reportItems
    // );
  }

  onCheckChange(ev){
    console.log('ch', ev);
    this.filter =  ev;
  }

  selectPreprocessing(event){
    this.preProcessingSelectedValue = event.target.value; 
  }

  selectReportType(event){
    this.selectReportType = event.target.value; 

  }
  /**
 * convertFile() - convert the file into base64 string format
 * @param event 
 */
  convertFile(event: any) {
    this.convertFileToBase64(event);
    this.extractedContent = '';
    this.path = '';
    const len = event.target.files[0].name.split('.').length;
    if (event.target.files[0].name.split('.')[len - 1] !== 'csv') {
    if (event.target.files && event.target.files[0]) {
      this.fileName = event.target.files[0].name;
      this.fileString = event.target.files;
      console.log('this.fileName', this.fileName, this.fileString);
    }  else {
        this.commonFunctionality(true, true, 'Result...');
      }
    } else {
      this.notify.warn('', 'Please choose other than csv file');
    }
  }
  convertFileToBase64(event) {
    if (event.target.files && event.target.files[0]) {
      this.commonFunctionality(false, false, 'Result...');
      var reader = new FileReader();
      reader.onload = (event: any) => {
        var binaryString = event.target.result;
        this.SelectedImageAsBase64 = binaryString;
      }
      reader.readAsDataURL(event.target.files[0]);
    } else {
      this.commonFunctionality(true, true, 'Result...');
    }
  }
  convertCSVFile(event: any) {
    let len = event.target.files[0].name.split('.').length;
    if (event.target.files[0].name.split('.')[len - 1] === 'csv') {
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        this.commonFunctionality(false, false, 'Result...');
        var reader = new FileReader();
        reader.onload = (event: any) => {
          var binaryString = event.target.result;
          this.fileString = binaryString.substring(binaryString.indexOf("base64,") + 7);
        }
        reader.readAsDataURL(event.target.files[0]);
      } else {
        this.commonFunctionality(true, true, 'Result...');
      }
    } else {
      this.notify.warn('', 'Please use only csv file');
    }
  }

  /**
   * analysis() - compare with defaultOption freetext/documents based on that,
   * it wil create a defaultObj and url for the freetext and archieve test,
   * when analysis processed and get the refId as response, it will move to dashboard
   * @param val - string 
   */
  analysis(val: string) {
    let defaultObj = {};
    let url: string;
    this.loading = true;
    if (this.fileName === undefined || this.fileName === "")
    {
      this.notify.error('', "Please select image");
      this.loading = false;
      return;
    }
    if (this.defaultOption === 'documents') {
      defaultObj = {
        'file': this.fileString,
        'option': this.preProcessingSelectedValue,
        'report_type': this.selectReportType,
        // 'filename': this.fileName
      }
      url = 'imageocr';
    } else if (this.defaultOption === 'archieve') {
      defaultObj = {
        'file': this.fileString,
        // 'filename': this.fileName,
      }
      url = 'licenseplate';
    }
    if (url === 'licenseplate'){
      this.defaultService.imageOCR(defaultObj, url).subscribe(res => {
        if (res) {
          this.path = '';
          this.extractedContent = '';
          this.extractedText = '';
          this.path = 'lpr';
          let imgname = res.filepath.split("/")[5];
          this.extractedContent = this.base_url + 'assets/lpr/' + res.filepath;
          this.extractedText = res.text;
          this.result = 'Predicted:' + ' ' + JSON.stringify(res.result).toUpperCase().replace(/"/g, '');
          this.imageResult = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
          + res.filepath);
          this.extractedContent = this.imageResult;
          this.outputFileName = res.download_file_name;
        }
        this.loading = false;
      }, error => {
        if (error.status == 504 && error.action == 1) {
          this.notify.error('', error.message);
        }
        this.loading = false;
      })
    }else{
      console.log('defaultObj', defaultObj);
      this.defaultService.analysisProcess(defaultObj, url).subscribe(res => {
        console.log('res', res);
        this._compiler.clearCache();
        this.path = '';
        this.extractedContent = null;
        this.extractedText = '';
        this.path = 'ocr';
        this.extractedContent = null;
        setTimeout(() => {
          let imgname = res.filepath.split("/")[5];
          console.log('imgname', imgname);
          this.extractedContent = this.base_url + 'assets/ocr/' + res.filepath;
          this.extractedText = res.text;
          this.loading = false;
          this.imageResult = this._sanitizer.bypassSecurityTrustResourceUrl(`${this.base_url}`
          + res.filepath);
          console.log('imageResult', JSON.stringify(this.imageResult));
          this.extractedContent = this.imageResult;
          this.outputFileName = res.download_file_name;
        }, 1000);
          // this.extractedContent = res.picture;
      }, error => {
        if (error.status == 504 && error.action == 1) {
          this.notify.error('', error.message);
        }
        this.loading = false;
      })
    }
  }

  downloadReport(val: string)
  {
    let defaultObj = {};
    let url: string;
    this.loading = true;
      defaultObj = {
        'filename': this.outputFileName
      } 
      this.defaultService.downloadReport(defaultObj).subscribe(res => {
        if (res) 
        {
          this.downloadableFileName = res.filename;
          this.downloadFile(res.filedata);
        }
        this.loading = false;
      }, error => {
        if (error.status == 504 && error.action == 1) {
          this.notify.error('', error.message);
        }
        this.loading = false;
      })   
  }
  downloadFile(data: any)
  {
    if (data) {
      var blob = this.base64ToBlob(data, 'text/plain');
      // const blob = new Blob([data], {
      // type: '' + data,
      // });
      const url = window.URL.createObjectURL(blob);
      const anchor = document.createElement('a');
      anchor.download = this.downloadableFileName;
      anchor.href = url;
      anchor.click();
    }
     
  }

  public base64ToBlob(b64Data, contentType= '', sliceSize= 512) {
    b64Data = b64Data.replace(/\s/g, ''); //IE compatibility...
    let byteCharacters = atob(b64Data);
    let byteArrays = [];
    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        let slice = byteCharacters.slice(offset, offset + sliceSize);

        let byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        let byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }
    return new Blob(byteArrays, {type: contentType});
}

  /**
   * restrict() - to disabled/enabled the buttons based 
   * on the freeText content length
   * @param ev 
   */
  restrict(ev: any) {
    if (this.freeText.length < 1) {
      this.commonFunctionality(true, true, 'Result...');
    } else {
      this.commonFunctionality(false, false, 'Result...');
    }
  }

  defaultDisable(val: string) {
    this.defaultOption = val;
    if (this.defaultOption === 'documents') {
      this.defaultTextArea = true;
      this.isCSVDisable = true;
      this.isOCR = true;
      this.path = '';
      this.extractedText = '';
      this.extractedContent = '';
    } else if (this.defaultOption == 'archieve') {
      this.defaultTextArea = true;
      this.isCSVDisable = true;
      this.isOCR = false;
      this.path = '';
      this.extractedContent = '';
      this.extractedText = '';

    } else {
      this.defaultTextArea = false;
      this.isCSVDisable = true;
      this.isOCR = false;
      this.path = '';
      this.extractedContent = '';
      this.extractedText = '';
    }
    this.clear();
  }

  clear() {
    this.commonFunctionality(true, true, 'Result...');
    this.freeText = null;
    this.fileName = '';
    this.fileString = '';
    this.myInput.nativeElement.value = '';
    this.csvInput.nativeElement.value = '';
    this.extractedText = '';
    this.extractedContent = '';
    this.path = '';
    this.SelectedImageAsBase64 = '';
    this.reset();

   }


   reset()
   {
    // var selectedValues=[];
    // selectedValues=["","",""];
    // window.location.reload();
    this.preProcessingDropDown.nativeElement.value = "";
    this.docTypeDropDown.nativeElement.value = "";
   }

  commonFunctionality(val1: boolean, val2: boolean, val3: string) {
    this.btnDisabled = val1;
    this.btnClear = val2;
    this.result = val3;
  }

}
