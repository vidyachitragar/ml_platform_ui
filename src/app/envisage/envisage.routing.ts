import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ArchieveDocumentComponent } from './archieve-document/archieve-document.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { DefaultComponent } from './default/default.component';

const ephiRoute: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: '',
                component: DefaultComponent
            },
            {
                path: 'archieve',
                component: ArchieveDocumentComponent
            },
            {
                path: 'dashboard',
                component: DashboardComponent
            }
           
        ],
    }

]


@NgModule({
    imports: [
        RouterModule.forChild(ephiRoute)
    ],
    exports: [RouterModule]
})

export class EnviRoutingModule { }