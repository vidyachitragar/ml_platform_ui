import { Injectable } from '@angular/core';
import { Http, ConnectionBackend, RequestOptions, RequestOptionsArgs, Response, 
  Headers, Request, XHRBackend } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/Rx';
import { tap, catchError, finalize } from 'rxjs/operators';

@Injectable()
export class InterceptorsService extends Http {

  private requestCount = 0;
  constructor(backend: ConnectionBackend, defaultOptions: RequestOptions) {
    super(backend, defaultOptions);
  }

  get(url: string, options?: RequestOptionsArgs): Observable<any> {
    return super.get(url, this.getOptions(options))
    .pipe(tap(response => this.checkStatus(response)), 
    catchError(err => this.logError(err)), finalize(() => {  }));
  }

  post(url: string, body: any, options?: RequestOptionsArgs): Observable<any> {
    console.log("Post");
    return super.post(url, body, this.getOptions(options))
    .pipe(tap(response => this.checkStatus(response)), 
    (catchError(err => this.logError(err))), (finalize(() => {  })));
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<any> {
    return super.delete(url, this.getOptions(options))
    .pipe(tap(response => this.checkStatus(response)), 
    (catchError(err => this.logError(err))), (finalize(() => { })));
  }

  private getOptions(options?: RequestOptionsArgs): RequestOptionsArgs {
    let requestOptions: RequestOptionsArgs = options || new RequestOptions({ headers: new Headers() });
    return requestOptions;
  }

  private checkStatus(response: any): void {
    if (response.status != 200) {
    }
  }

  private logError(ex: any): any {
    console.log(ex);
    // Bad Request
    if (ex.status == 400) {
      this.logBadRequest(ex);
    }
    else {
      let exception = JSON.parse(ex._body);
      let message: string = ex.status + ": " + exception.error + "\t path : " + exception.path;
    }

    return Observable.throw(ex);
  }

  private logBadRequest(ex: any) {
    console.log(ex._body);
    let message: string = ex.status + ": " + ex._body;
  }
}

export function ServiceFactory(backend: XHRBackend, defaultOptions: RequestOptions) {
  return new InterceptorsService(backend, defaultOptions);
}

