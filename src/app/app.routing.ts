import { Routes } from '@angular/router';
import { HomeComponent } from './ephiphany/home/home.component';
import { FreetextComponent } from './ephiphany/freetext/freetext.component';
import { LandingComponent } from './landing/landing.component';


export const AppRoutes: Routes = [
    { path: 'home', component: LandingComponent },
    {
        path: 'envisage',
        loadChildren: './envisage/envisage.module#EnviModule'
       
    }
    ,{
        path: 'epiphany',
        loadChildren: './ephiphany/ephiphany.module#EphiModule'
    }
    ,{path: '', redirectTo: '/envisage', pathMatch: 'full'}
];