import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NvD3Module } from 'ng2-nvd3';

import { SpinnerComponent } from './components/spinner/spinner.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { GaugeComponent } from './charts/gauge/gauge.component';
import { StackbarComponent } from './charts/stackbar/stackbar.component';
import { LineComponent } from './charts/line/line.component';
// d3 and nvd3 should be included somewhere
import 'd3';
import 'nvd3';
import { LineChartDataFormat, GaugeDataFormat, StackChartDataFormat } from './pipes/common.pipe';
const PIPE = [ LineChartDataFormat, GaugeDataFormat, StackChartDataFormat];
@NgModule({
    imports: [
        CommonModule,
        NvD3Module
    ],
    declarations: [SpinnerComponent, 
        DropdownComponent, 
        GaugeComponent, 
        StackbarComponent, 
        LineComponent,
        PIPE],
    exports: [SpinnerComponent,
        DropdownComponent,
        GaugeComponent,
        StackbarComponent,
        LineComponent,
        PIPE],
    providers: [],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA,
    ],
})
export class UtilsModule { }
