import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  public refId: string;

  // public refId: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  // public value: string;
  // constructor() { }

  // setRefId(refId: string) {
  //   this.refId.next(refId);
  // }

  setRefId(refId: string){
    this.refId = refId;
  }
}
