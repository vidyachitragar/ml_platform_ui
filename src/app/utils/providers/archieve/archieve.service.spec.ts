import { TestBed } from '@angular/core/testing';

import { ArchieveService } from './archieve.service';

describe('ArchieveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ArchieveService = TestBed.get(ArchieveService);
    expect(service).toBeTruthy();
  });
});
