import { Injectable } from '@angular/core';
import { Headers,RequestOptions, Http } from '@angular/http';
import { Observable, throwError } from "rxjs";
import { catchError, map } from 'rxjs/operators';
import { AppWebUrlsService } from '../../../core/providers/app-web-urls.service';
import { ErrortransformService } from '../../../core/providers/errortransform.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DefaultService {
  private http;
  constructor( 
    private httpclient: HttpClient,
    private appWebUrlsService: AppWebUrlsService,
    private errortransformService: ErrortransformService) { 
      this.http = this.httpclient;
  }

    /**
   *  analysisProcess() - POST method, get the text analysis response
   * @param val Text
   */
  analysisProcess(val, url): Observable<any>{
    let body = val;
    const formData: FormData = new FormData();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'multipart/form-data'
      })
    };
    formData.append('file', body.file[0]);
    formData.append('option', body.option);
    console.log('body', body, httpOptions, formData);
    return this.http.post(`${environment.apiEndPoint}camscanner/scanner/`, formData)
    .pipe(
      map(res => {
        return res
      }),
      catchError(apiError =>{
        let transformedError = this.errortransformService.transformError(apiError);
        return Observable.throw(transformedError);
      })
    )
  }

  getPreprocessing(): Observable<any>
  {
    // let headers = new Headers({'Content-Type': 'application/json'});
    // let options = new RequestOptions({headers:headers});
    return this.http.get(`${environment.apiEndPoint}camscanner/options/`)
    .pipe(
      map( (res: any) => {
        // return res;
        return res.options;
      }),
      catchError(apiError =>{
        let transformedError = this.errortransformService.transformError(apiError);
        return Observable.throw(transformedError);
      })
    )
  }

  getReportType(): Observable<any>
  {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers:headers});
    return this.http.get(`${this.appWebUrlsService.fullUrl}report`, options)
    .pipe(
      map( (res: any) => {
        // return res;
        return res.report;
      }),
      catchError(apiError =>{
        let transformedError = this.errortransformService.transformError(apiError);
        return Observable.throw(transformedError);
      })
    )
  }

  imageOCR(val, url): Observable<any>{
    let body = val;
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers:headers});
    return this.http.post(`${this.appWebUrlsService.fullUrl}${url}`, body, options)
    .pipe(
      map(res => {
        return res
      }),
      catchError(apiError =>{
        let transformedError = this.errortransformService.transformError(apiError);
        return Observable.throw(transformedError);
      })
    )
  }

  //Download report API call
  downloadReport(val):any{
    let body = val;
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers:headers});
    return this.http.post(`${this.appWebUrlsService.fullUrl}download_file`,body, options)
    .pipe(
      map(res => {
        return res
      }),
      catchError(apiError =>{
        let transformedError = this.errortransformService.transformError(apiError);
        return Observable.throw(transformedError);
      })
    )
  }

}
