import { Component, OnInit, Input, ViewChild } from '@angular/core';
declare let d3: any;

@Component({
  selector: 'line-chart',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.css']
})
export class LineComponent implements OnInit {
  options: any;
  data: any;
  @Input('config') config;
  @Input('lineDataInput') lineDataInput;
  @ViewChild('nvd3') nvd3;
  
  constructor() { }

  ngOnInit() {
    const month = ['oct-18', 'Nov-18', 'Dec-18', 'Jan-19', 'Feb-19', 'Mar-19', 'Apr-19', 'May-19', 'Jun-19', 'Jul-19', 'Aug-19', 'Sep-19'];
    this.data = this.lineDataInput;

    var label = [];
    var val = [];
    this.lineDataInput.forEach((el, i) => {
        if (i == 0) {
            el.values.forEach((ele, j) => {
                label.push(ele.label);
            });
        }
    });
    this.lineDataInput.forEach((el, i) => {
        if (i == 0) {
            el.values.forEach((ele, j) => {
                val.push(ele.value);
            });
        }
    });
    // LineChart
    const that = this;
    this.options = {
      chart: {
        type: 'lineChart',
        height: this.config.height,
        color: ['#d346b1', '#1f8ef1', '#00d6b4'],
        showLegend: true,
        legendPosition: 'bottom',
        // noData: 'Click on Line chart to display Data',
        margin: {
          top: 20,
          right: 60,
          bottom: 70,
          left: 65
        },
        x: function (d, i) { return i; },
        y: function (d) { return d.value; },
        useInteractiveGuideline: true,
        reduceXTicks: false,
        showXAxis: true,
        xAxis: {
          ticks: 10,
          showMaxMin: false,
          rotateLabels: -40,
          tickFormat: function (d) {
            return label[d];
          },
        },
        yAxis: {
          tickFormat: function (d) {
            return d;
          },
          axisLabelDistance: -10
        },
        //To Show Data Markers
                callback: function () {
                    d3.selectAll('.nv-lineChart .nv-point')
                        .style("stroke-width", "5px")
                        .style("fill-opacity", ".95")
                        .style("stroke-opacity", ".95");
                    d3.selectAll('.nv-legend-text').style('fill', '#898a8d');
                    d3.selectAll('nv-axisMaxMin nv-axisMaxMin-y nv-axisMin-y')
                    .append('text')
                    .style('fill', '#898a8d');
                },
      }
    };
  }

  ngOnChanges() {
    this.data = this.lineDataInput;
  }

}
