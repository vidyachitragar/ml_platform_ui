export const GAUGEDATA = [{
    "pv": 10,
    "nv": 20,
    "nu": 40,
    "div": '#gauge-chart-0 svg'
}]

export const LCDATA = [{
  'key': "FY 2017",
  'values': [{
      'x': 0,
      'y': 20,
      'key': "Value1"
  },
  {
      'x': 1,
      'y': 50,
      'key': "Value2"

  },
  {
      'x': 2,
      'y': 40,
      'key': "Value3"
  },
  {
      'x': 3,
      'y': 90,
      'key': "Value4"
  },{
    'x': 4,
    'y': 20,
    'key': "Value5"
},
{
    'x': 5,
    'y': 65,
    'key': "Value6"
},
{
    'x': 6,
    'y': 110,
    'key': "Value7"

}]
},{
  'key': "FY 2018",
  'values': [{
      'x': 0,
      'y': 12,
      'key': "Value1"
  },
  {
      'x': 1,
      'y': 86,
      'key': "Value2"

  },
  {
      'x': 2,
      'y': 42,
      'key': "Value3"
  },
  {
      'x': 3,
      'y': 50,
      'key': "Value4"
  },
  {
      'x': 4,
      'y': 20,
      'key': "Value5"
  },
  {
      'x': 5,
      'y': 65,
      'key': "Value6"
  },
  {
      'x': 6,
      'y': 110,
      'key': "Value7"

  },
  {
      'x': 7,
      'y': 70,
      'key': "Value8"
  },
  {
      'x': 8,
      'y': 23,
      'key': "Value9"
  },
  {
      'x': 9,
      'y': 10,
      'key': "Value10"
  },
  {
      'x': 10,
      'y': 99,
      'key': "Value11"
  },
  {
      'x': 11,
      'y': 85,
      'key': "Value12"
  }]
  },
  {
      'key': "FY 2019",
      'values': [{
          'x': 0,
          'y': 20,
          'key': "Value1"
      },
      {
          'x': 1,
          'y': 50,
          'key': "Value2"

      },
      {
          'x': 2,
          'y': 40,
          'key': "Value3"
      },
      {
          'x': 3,
          'y': 90,
          'key': "Value4"
      }]
  }
];
 export const SBCDATA =  [ {
  "key": "Active Members",
  "values": [
    {
      "name": "FY 2016",
      "value": 79
    },
    {
      "name": "FY 2017",
      "value": 104
    },
    {
      "name": "FY 2018",
      "value": 80
    }
  ]
},
{
  "key": "Local Sponsor",
  "values": [
    {
      "name": "FY 2016",
      "value": 40
    },
    {
      "name": "FY 2017",
      "value": 25
    },
    {
      "name": "FY 2018",
      "value": 34
    }
  ]
},
{
  "key": "National Sponsor",
  "values": [
    {
      "name": "FY 2016",
      "value": 36
    },
    {
      "name": "FY 2017",
      "value": 58
    },
    {
      "name": "FY 2018",
      "value": 30
    }
  ]
}];
		