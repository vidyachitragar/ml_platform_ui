import { Pipe, PipeTransform } from '@angular/core';

// formatting lineChart data
@Pipe({name: 'lineChartDataFormat'})
export class LineChartDataFormat implements PipeTransform {
  transform(val: any): any {
    let keySet1 = [], keySet2 = [], keySet3 = [], finalKeySet = [];
    let keyString1 : any, keyString2 : any, keyString3 : any;
    val.forEach(el => {
        if( el[1] == "positive" || el[1] == "negative" || el[1] == "neutral" ) {
            if(el[1] == "positive"){
                keySet1.push({"label":el[0],"value":el[2]})
                keyString1 = "+ve";
            } 
            else if(el[1] == "negative"){
                keySet2.push({"label":el[0],"value":el[2]})
                keyString2 = "-ve";
            }else if(el[1] == "neutral"){
                keySet3.push({"label":el[0],"value":el[2]})
                keyString3 = "NEUTRAL";
            }
        } else if(el[1] == "spam" || el[1] == "ham" || el[1] == "None"){
            if(el[1] == 'spam'){
                keySet1.push({'label': el[0], 'value': el[2]});
                keyString1 = "SPAM"
            } else if( el[1] == 'ham'){
                keySet2.push({'label': el[0], 'value': el[2]});
                keyString2 = "HAM";
            } else if( el[1] == 'None'){
                keySet3.push({'label': el[0], 'value': el[2]});
                keyString3 = "NONE";
            }
        } else if(el[1] == "Threat" || el[1] == "No Threat"){
            if(el[1] == 'Threat'){
                keySet2.push({'label': el[0], 'value': el[2]});
                keyString2 = "THREAT"
            } else if( el[1] == 'No Threat'){
                keySet1.push({'label': el[0], 'value': el[2]});
                keyString1 = "NO THREAT";
            }
        }
    }); 
    
    if(keyString1 == "SPAM"){
        finalKeySet.push({ "key": keyString1, "values": keySet1 },
        { "key": keyString2, "values": keySet2});
        return finalKeySet;
    } else if(keyString1 == "+ve") {
        finalKeySet.push({ "key": keyString1, "values": keySet1 },
        { "key": keyString2, "values": keySet2},
        { "key": keyString3, "values": keySet3});
        return finalKeySet;
    } else if(keyString1 == "NO THREAT") {
        finalKeySet.push({ "key": keyString1, "values": keySet1},
        { "key": keyString2, "values": keySet2});
        // finalKeySet.push({"key": keyString1, values: keySet1});
        return finalKeySet;
    }
  
  }
}

// formatting gaugeChart data
@Pipe({name: 'gaugeDataFormat'})
export class GaugeDataFormat implements PipeTransform {
  transform(val: any): any {
    let keySet1 = 0, keySet2 = 0 , keySet3 = 0, i = 0;
    let finalKeySet = [];

    val.forEach(el => {
        if(el[1] == "positive"){
            keySet1+=parseInt(el[2])
            ++i
        } else if(el[1] == "negative") {
            keySet2 += parseInt(el[2])
            ++i
        } else if(el[1] == "neutral") {
            keySet3 += parseInt(el[2])
            ++i
        }
    });
    let total =  keySet1 + keySet2 + keySet3;
    let t = total / 100;
    finalKeySet.push({"div": "#gauge-chart-0 svg","pv":Math.round((keySet1 / t)),
    "nv":Math.round((keySet2 / t)),"nu":Math.round((keySet3 / t))})
    return finalKeySet;
  }
}

// formatting stackChart data
@Pipe({name: 'stackChartDataFormat'})
export class StackChartDataFormat implements PipeTransform {
    transform(val: any): any {
        let keySet1= 0, keySet2 = 0;
        let dataSet1 = [], dataSet2 = [], finalKeySet = [];
        let keyString1 : any, keyString2 : any;

        val.forEach(el => {
            if(el[1] == "spam" || el[1] == "ham" || el[1] == "None"){
                if(el[1] == "spam"){
                    keySet1+= parseInt(el[2])
                    keyString1 = "SPAM";
                } else if(el[1] == "ham"){
                    keySet2+= parseInt(el[2])
                    keyString2 = "HAM";
                } 
            } else if(el[1] == "Threat" || el[1] == "No Threat"){
                if(el[1] == "No Threat"){
                    keySet1+= parseInt(el[2])
                    keyString1 = "NO THREAT";
                } else if(el[1] == "Threat"){
                    keySet2+= parseInt(el[2])
                    keyString2 = "THREAT";
                }
            }
        });

        if(keyString1 == 'SPAM') {
            dataSet1.push({"name":"","value":keySet1});
            dataSet2.push({"name":"","value":keySet2});
            finalKeySet.push({"key":keyString1,"values":dataSet1});
            finalKeySet.push({"key":keyString2,"values":dataSet2});
            return finalKeySet;
        } else if(keyString1 == 'NO THREAT'){
            dataSet1.push({"name":"","value":keySet1});
            dataSet2.push({"name":"","value":keySet2});
            finalKeySet.push({"key":keyString1,"values":dataSet1});
            finalKeySet.push({"key":keyString2,"values":dataSet2});
            return finalKeySet;
        }

    }
}
