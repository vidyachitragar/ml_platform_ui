import { Component, OnInit, ElementRef, AfterViewInit } from '@angular/core';
import { FreetextService } from '../../utils/providers/freetext/freetext.service';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-freetext',
  templateUrl: './freetext.component.html',
  styleUrls: ['./freetext.component.css']
})
export class FreetextComponent implements OnInit, AfterViewInit {
  result: string = 'Result...';
  analytics: boolean = true;
  loading: boolean = true;
  freeText: string;

  /**
  *This is invoked when Angular creates a component or directive by calling new on the class.
  * @param freetextService is for FreetextService, 
  * @param notify is for Notification( success, error, warning) show or hide 
  */
  constructor(private elementRef: ElementRef,
    private notify: NotificationsService,
    private freetextService:FreetextService) { 
      setTimeout(()=>{
        this.loading = false;
      },1000)
    }

  ngOnInit() {
  }

  ngAfterViewInit(){
    // this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = 'white';
 }

 analyse(){
   if(this.freeText == ''){
    this.notify.warn('','Enter a text to process');
   } else {
    this.loading = true;
    let textObj = {};
    textObj = {
      'content': this.freeText
    }
    this.freetextService.textProcess(textObj).subscribe(res => {
      this.responseMessage(res);
      this.result = res.result;
    }, error => {
      console.log("Error", error);
      if(error.ok == false){
        this.loading = false;
        // this.result = 'HELLO';
      }
    });
  }
 }

   /**
   * clear() is used for clear the filename and result
   */
  clear(){
    this.freeText = null;
    this.result = 'Result...';
  }

  /**
  * Response and Error messages show
  */
  responseMessage(res) {
    this.loading = false;
    console.log("Response", res);
  }

}
