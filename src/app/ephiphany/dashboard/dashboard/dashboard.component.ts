import { Component, OnInit, ElementRef, AfterViewInit } from '@angular/core';
// import { GAUGEDATA, LCDATA, SBCDATA } from '../../../utils/mock/data';
import { DashboardAnalysisService } from '../../../utils/providers/dashboard/dashboard.service';
import { CommonService } from '../../../utils/providers/common/common.service';
import { NotificationsService } from 'angular2-notifications';
import { data } from 'src/app/utils/mock/mock';
import { info } from 'src/app/utils/mock/info';
import { LineChartDataFormat, GaugeDataFormat, StackChartDataFormat } from '../../../utils/pipes/common.pipe';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  gaugeData: any;
  lineConfig: any;
  stackConfig: any;
  lineData: any;
  stackData: any;
  seaLineData: any;
  spamLineData: any;
  infoLineData: any;
  spamStackData: any;
  infosecStackData: any;
  loading: boolean = true;
  lineChart = new LineChartDataFormat();
  gaugeChart = new GaugeDataFormat();
  stackChart = new StackChartDataFormat();
  isseaImage: boolean;
  isspamImage: boolean;
  isinfoImage: boolean;

  /**
  *This is invoked when Angular creates a component or directive by calling new on the class.
  * @param analysisService is for Dashboard Analysis Data, 
  * @param notify is for Notification( success, error, warning) show or hide 
  */
  constructor(private elementRef: ElementRef,
    private analysisService: DashboardAnalysisService,
    private commonService: CommonService,
    private notify: NotificationsService) {
      setTimeout(()=>{
        this.loading = false;
      },1000)
     }

  /**
  * Invoked when given component has been initialized. 
  */
  ngOnInit() {
    this.analysisData();
    // this.lineData = LCDATA;
    // this.spamStackData = SBCDATA;
    // this.gaugeData = GAUGEDATA;
    this.lineConfig = {
      height : 300,  
      xAxisLabel : "",
      yAxisLabel : "No. of Mails"
    }
  }

  ngAfterViewInit(){
    // this.elementRef.nativeElement.ownerDocument.body.style.backgroundColor = '#1e1e2b';
 }

/**
 * analysisData() is to show the visualization of data 
 */
 analysisData(){
    console.log("dash", data[0]);
    this.seaLineData = this.lineChart.transform(data[0].sea);
    this.spamLineData = this.lineChart.transform(data[0].spams);
    this.infoLineData = this.lineChart.transform(data[0].infosec);
    this.gaugeData = this.gaugeChart.transform(data[0].sea);
    this.spamStackData = this.stackChart.transform(data[0].spams);
    this.infosecStackData = this.stackChart.transform(data[0].infosec);
   this.analysisService.getDashboardData().subscribe(res=>{
     console.log("Dashboard", res);
    // this.seaLineData = this.lineChart.transform(res.sea);
    // this.spamLineData = this.lineChart.transform(res.spams);
    // this.infoLineData = this.lineChart.transform(res.infosec);
    // this.gaugeData = this.gaugeChart.transform(res.sea);
    // this.spamStackData = this.stackChart.transform(res.spams);
    // this.infosecStackData = this.stackChart.transform(res.infosec);
   }, error => {
     console.log("Dashboard Error", error);
  })
 }

 wordCloudView(val: string){
  if( val == 'sea'){
    this.isseaImage = true;
    this.isspamImage = false;
    this.isinfoImage = false;
  } else if( val == 'spam'){
    this.isseaImage = false;
    this.isspamImage = true;
    this.isinfoImage = false;
  } else if(val == 'info'){
    this.isseaImage = false;
    this.isspamImage = false;
    this.isinfoImage = true;
  }
 }

}
