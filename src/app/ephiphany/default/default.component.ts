import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import { DefaultService } from '../../utils/providers/default/default.service';
import { CommonService } from '../../utils/providers/common/common.service';
import { Router } from '@angular/router';
import {SliderModule} from 'primeng/slider';
@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.css']
})
export class DefaultComponent implements OnInit {
  @ViewChild('myInput') myInput: ElementRef;
  @ViewChild('csvInput') csvInput: ElementRef;
  @ViewChild('summaryInput') summaryInput: ElementRef;
  loading: boolean = true;
  btnDisabled: boolean = true;
  btnClear: boolean = true;
  defaultTextArea: boolean;
  isCSVDisable: boolean = true;
  isOCR: boolean = false;
  isSummary: boolean;
  extractedContent: string;
  extractedsummary: string;
  result: string = 'Result...';
  defaultOption: string = 'freetext';
  freeText: string;
  fileName: any;
  sumpercent: any = 10;
  fileString: any;
  docsent: string;
  sumsent: string;

  constructor(private router: Router,
    private notify: NotificationsService,
    private defaultService: DefaultService,
    private commonService: CommonService) {
    setTimeout(() => {
      this.loading = false;
    }, 1000)
  }

  ngOnInit() {
  }

  /**
 * convertFile() - convert the file into base64 string format
 * @param event 
 */
  convertFile(event: any) {
    let len = event.target.files[0].name.split('.').length;
    if (event.target.files[0].name.split('.')[len - 1] != 'csv') {
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        this.commonFunctionality(false, false, 'Result...');
        var reader = new FileReader();
        reader.onload = (event: any) => {
          var binaryString = event.target.result;
          this.fileString = binaryString.substring(binaryString.indexOf("base64,") + 7);
        }
        reader.readAsDataURL(event.target.files[0]);
      } else {
        this.commonFunctionality(true, true, 'Result...');
      }
    } else {
      this.notify.warn('', 'Please choose other than csv file');
    }
  }

  convertsummaryFile(event: any) {
    let len = event.target.files[0].name.split('.').length;
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        this.commonFunctionality(false, false, 'Result...');
        var reader = new FileReader();
        reader.onload = (event: any) => {
          var binaryString = event.target.result;
          this.fileString = binaryString.substring(binaryString.indexOf("base64,") + 7);
        }
        reader.readAsDataURL(event.target.files[0]);
      } else {
        this.commonFunctionality(true, true, 'Result...');
      }
  }

  convertCSVFile(event: any) {
    let len = event.target.files[0].name.split('.').length;
    if (event.target.files[0].name.split('.')[len - 1] === 'csv') {
      if (event.target.files && event.target.files[0]) {
        this.fileName = event.target.files[0].name;
        this.commonFunctionality(false, false, 'Result...');
        var reader = new FileReader();
        reader.onload = (event: any) => {
          var binaryString = event.target.result;
          this.fileString = binaryString.substring(binaryString.indexOf("base64,") + 7);
        }
        reader.readAsDataURL(event.target.files[0]);
      } else {
        this.commonFunctionality(true, true, 'Result...');
      }
    } else {
      this.notify.warn('', 'Please use only csv file');
    }
  }

  analysisSummray(val:string){
    let defaultObj = {};
    let url: string;
    defaultObj = {
      'content': this.fileString,
      'filename': this.fileName,
      'sumpercent': this.sumpercent,
      'option': val
    }
    url = 'summaryfromfile';
    this.defaultService.analysisProcess(defaultObj, url).subscribe(res => {
        this.extractedContent = res.content;
        this.extractedsummary = res.summary;
        this.docsent = 'No. of Sentences = ' + res.docsent;
        this.sumsent = 'No. of Sentences = ' + res.sumsent;

        this.result = 'Predicted:' + ' ' + JSON.stringify(res.result).toUpperCase().replace(/"/g, '');
        this.clear();
      
    }, error => {
      if (error.status == 504 && error.action == 1) {
        this.notify.error('', error.message);
      }
    })
  }

  /**
   * analysis() - compare with defaultOption freetext/documents based on that,
   * it wil create a defaultObj and url for the freetext and archieve test,
   * when analysis processed and get the refId as response, it will move to dashboard
   * @param val - string 
   */
  analysis(val: string) {
    this.loading = true;
    let defaultObj = {};
    let url: string;

    if (this.defaultOption === 'freetext') {
      defaultObj = {
        'content': this.freeText,
        'option': val
      }
      url = 'analyse_freetext';
    } else if (this.defaultOption === 'documents') {
      defaultObj = {
        'content': this.fileString,
        'filename': this.fileName,
        'option': val
      }
      url = 'analysefromfile';
    } else if (this.defaultOption === 'archieve') {
      defaultObj = {
        'content': this.fileString,
        'filename': this.fileName,
        'option': val
      }
      url = 'analysefromfile';
    }
  
    this.defaultService.analysisProcess(defaultObj, url).subscribe(res => {
      if (res.refid) {
      this.loading = false;
        this.notify.success('', 'Success');
        localStorage.removeItem('refid');
        this.commonService.setRefId(res.refid);
        localStorage.setItem('refid', res.refid);
        this.router.navigate(['/epiphany/dashboard']);
      } else {
      this.loading = false;

        this.extractedContent = res.content;
        this.extractedsummary = res.summary;
        this.result = 'Predicted:' + ' ' + JSON.stringify(res.result).toUpperCase().replace(/"/g, '');
      }
    }, error => {
      this.loading = false;

      if (error.status == 504 && error.action == 1) {
        this.notify.error('', error.message);
      }
    })

  }

  /**
   * restrict() - to disabled/enabled the buttons based 
   * on the freeText content length
   * @param ev 
   */
  restrict(ev: any) {
    if (this.freeText.length < 1) {
      this.commonFunctionality(true, true, 'Result...');
    } else {
      this.commonFunctionality(false, false, 'Result...');
    }
  }

  defaultDisable(val: string) {
    this.defaultOption = val;
    if (this.defaultOption === 'documents') {
      this.defaultTextArea = true;
      this.isCSVDisable = true;
      this.isSummary = false;
      this.isOCR = true;
    } else if (this.defaultOption == 'archieve') {
      this.defaultTextArea = false;
      this.isCSVDisable = false;
      this.isSummary = false;
    } else if (this.defaultOption == 'summary'){
      this.defaultTextArea = false;
      this.isCSVDisable = false;
      this.isSummary = true;
    }
     else {
      this.defaultTextArea = false;
      this.isCSVDisable = true;
      this.isSummary = false;
      this.isOCR = false;

    }
    this.clear();
  }

  clear() {
    this.commonFunctionality(true, true, 'Result...');
    this.freeText = null;
    this.fileName = '';
    this.fileString = '';
    this.myInput.nativeElement.value = '';
    this.csvInput.nativeElement.value = '';
    this.summaryInput.nativeElement.value = '';
    this.extractedContent = '';
    this.extractedsummary = '';
    this.docsent= '';
    this.sumsent = '';
    
  }

  commonFunctionality(val1: boolean, val2: boolean, val3: string) {
    this.btnDisabled = val1;
    this.btnClear = val2;
    this.result = val3;
  }

}
