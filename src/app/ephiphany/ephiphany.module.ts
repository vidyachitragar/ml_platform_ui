import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule, FormBuilder } from '@angular/forms';

import { EphiRoutingModule } from './ephiphany.routing';
import { HomeComponent } from './home/home.component';
import { FreetextComponent } from './freetext/freetext.component';
import { HeaderComponent } from './header/header.component';
import { ArchieveDocumentComponent } from './archieve-document/archieve-document.component';
import { ArchieveService } from '../utils/providers/archieve/archieve.service';
import { DefaultService } from '../utils/providers/default/default.service';
import { UtilsModule } from '../utils/utils.module';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { DefaultComponent } from './default/default.component';
import { SummaryComponent } from './summary/summary.component';
import {SliderModule} from 'primeng/slider';

@NgModule({
    imports: [ CommonModule, 
        ReactiveFormsModule, 
        FormsModule,
        UtilsModule,
        SliderModule,
        EphiRoutingModule],
    declarations: [
        HomeComponent,
        FreetextComponent,
        HeaderComponent,
        ArchieveDocumentComponent,
        DashboardComponent,
        DefaultComponent,
        SummaryComponent
        
    ],
    exports:[ ],
    providers:[ ArchieveService, DefaultService],
    schemas: [
      CUSTOM_ELEMENTS_SCHEMA,
        NO_ERRORS_SCHEMA
  ],
  })
  export class EphiModule { }