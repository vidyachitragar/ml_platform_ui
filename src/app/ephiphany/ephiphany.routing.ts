import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { FreetextComponent } from './freetext/freetext.component';
import { ArchieveDocumentComponent } from './archieve-document/archieve-document.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { DefaultComponent } from './default/default.component';
import { SummaryComponent } from './summary/summary.component';

const ephiRoute: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: '',
                component: DefaultComponent
            },
            {
                path: 'freetext',
                component: FreetextComponent
            },
            {
                path: 'archieve',
                component: ArchieveDocumentComponent
            },
            {
                path: 'dashboard',
                component: DashboardComponent
            },
            {
                path: 'summary',
                component: SummaryComponent
            }
           
        ],
    }

]


@NgModule({
    imports: [
        RouterModule.forChild(ephiRoute)
    ],
    exports: [RouterModule]
})

export class EphiRoutingModule { }